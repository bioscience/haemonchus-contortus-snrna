# Haemonchus contortus single nuclei RNA (snRNA) analysis

Code files for the publication "Single-nucleus RNA-seq analysis reveals the first cell atlas for Haemonchus contortus embryos and identifies two eukaryotic elongation factors as intervention target candidates".
To run, you have to download files in SRR24235293-SRR24235296 and update the run.sh accordingly.

## Description of Files
* `run.sh`: Main script that downloads the dependent data and controls the run for the single nuclei analysis 
* `runEmbryo.r`: R-script for the complete single nuclei analysis 
* `removeIsoforms.py`: Python3 script to help preprocess the files (called in run.sh)
* `divideByGenes.py`: Python3 script to help preprocess the files (called in run.sh)
