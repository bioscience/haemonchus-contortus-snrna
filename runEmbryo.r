library(Seurat)
library(stringr)
library(RColorBrewer)
library(SingleCellExperiment)
library(singleCellNet)
library(diem)
library(clustifyr)
library(scran)
library(scds)

############################################################
# Load the data
samplePath <- "./hcoEmbryoExons_150k/outs"
dataHc <- Read10X(paste(samplePath, "filtered_feature_bc_matrix", sep="/"))
hconSeurat <- CreateSeuratObject(counts = dataHc, min.cells = 3, min.features = 50, project = "EmbryoHc")
dim(dataHc) # 19473 150000
dim(hconSeurat) # 15490 83745
rownames(dataHc) <- str_replace(rownames(dataHc), "_", "-")
dataHc <- dataHc[rownames(hconSeurat),]
dim(dataHc) # 15490 150000
origHc <- dataHc

samplePath <- "."
dataCe <- Read10X(paste(samplePath, "MappedToHco", sep="/"))
celeSeurat <- CreateSeuratObject(counts = dataCe, min.cells = 3, min.features = 50, project = "EmbryoCe")
dim(dataCe) # 7897 89701
dim(celeSeurat) # 7852 89701
rownames(dataCe) <- str_replace(rownames(dataCe), "_", "-")
dataCe <- dataCe[rownames(celeSeurat),]
dim(dataCe) # 7852 89701

dataCe <- dataCe[intersect(rownames(dataHc), rownames(dataCe)),]
dim(dataCe) # 6980 89701
celeSeurat <- CreateSeuratObject(counts = dataCe, min.cells = 3, min.features = 50, project = "EmbryoCe")
dim(celeSeurat) # 6980 89701

# Cluster projections
cellAnnos <- read.table("GSE126954_cell_annotation.csv", header=TRUE, row.names=1, sep=",")
cellTypes <- as.matrix(cellAnnos[colnames(dataCe), 6, drop=FALSE])
cellTypes <- cellTypes[is.na(cellTypes) == FALSE,,drop=FALSE]
colnames(cellTypes) <- "cell_type1"
dataCe <- dataCe[,rownames(cellTypes)]

sceCele <- SingleCellExperiment(assays = list(counts = as.matrix(dataCe)), colData = cellTypes)
logcounts(sceCele) <- log2(counts(sceCele) + 1)
rowData(sceCele)$feature_symbol <- rownames(sceCele)
sceCele <- sceCele[!duplicated(rownames(sceCele)), ]
dim(sceCele) # 6980 54649
saveRDS(sceCele, file = "sceCeEmbryo.rds")
sceCele <- readRDS("sceCeEmbryo.rds")

################ Doublet filtering ##################
hconSeurat <- CreateSeuratObject(counts = origHc, min.cells = 3, min.features = 50, project = "EmbryoHc")
sce <- cxds(as.SingleCellExperiment(hconSeurat))
sce <- bcds(sce)
sce <- cxds_bcds_hybrid(sce)
CD <- colData(sce)
head(cbind(CD$cxds_score,CD$bcds_score, CD$hybrid_score))

doublets <- sce[, sce$hybrid_score > 0.8]
dim(doublets) # 15490 2149 2099
sce <- sce[, sce$hybrid_score <= 0.8]
dim(sce) # 15490 81596 81646

# Remove again genes that have no mapped reads
keep_feature <- rowSums(counts(sce) > 0) > 0
sce <- sce[keep_feature, ]
dim(sce) # 15490 81596 81646

hconSeurat <- as.Seurat(sce)

#############################
# Hcon Diem
sceHcon <- create_SCE(hconSeurat@assays$RNA@counts)
jpeg("barcodeRankPlot.jpg", width=1200, height=1000)
barcode_rank_plot(sceHcon)
dev.off()

sceHcon <- set_debris_test_set(sceHcon, top_n = Inf, min_counts = 200, min_genes = 200)
sceHcon <- filter_genes(sceHcon, cpm_thresh = 10)
genes <- gene_data(sceHcon)
summary(genes)

sceHcon <- get_pcs(sceHcon, n_var_genes=2000)
sceHcon <- init(sceHcon, k_init = 20, nstart_init = 30, min_size_init = 100, model = "mltn", seedn = 1, threads = 24, verbose = TRUE)
sceHcon <- run_em(sceHcon, max_iter = 1000, model = "mltn", threads = 84, verbose=TRUE)
drop_data <- droplet_data(sceHcon)
summary(drop_data)

sceHcon <- assign_clusters(sceHcon)
sceHcon <- estimate_dbr_score(sceHcon, thresh_genes=200)

sm <- summarize_clusters(sceHcon)
jpeg("debrisGenes.jpg", width=1200, height=1000)
plot_clust(sceHcon, feat_x = "n_genes", feat_y = "score.debris", log_x = TRUE, log_y = FALSE)
dev.off()

# Call targets by removing droplets in debris cluster(s) for single-cell data
sceHcon <- call_targets(sceHcon, clusters = "debris", thresh = NULL, min_genes = 200)
saveRDS(sceHcon, file = paste("sceHcEmbryoDiem.rds"))
sceHcon <- readRDS("sceHcEmbryoDiem.rds")
hconSeurat <- convert_to_seurat(sceHcon)

#############################
# Cluster H. contortus
hconSeurat <- NormalizeData(object = hconSeurat, normalization.method = "LogNormalize", scale.factor = 10000)
hconSeurat <- SCTransform(hconSeurat, verbose = FALSE)

pcaDim <- 20
# Run the standard workflow for visualization and clustering
hconSeurat <- RunPCA(hconSeurat, verbose = FALSE)
hconSeurat <- RunUMAP(hconSeurat, dims = 1:pcaDim)
jpeg(file="umapHcEmbryoDiemPca20.jpg", width=1200, height=1000)
DimPlot(hconSeurat, label = TRUE)
dev.off()

# Elbow plot shows that more PCA components could be used
jpeg(file="elbowHcEmbryoDiemSct.jpg", width=1200, height=1000)
ElbowPlot(object = hconSeurat, ndims=pcaDim)
dev.off()

hconSeurat <- FindNeighbors(hconSeurat, dims = 1:pcaDim, verbose = FALSE)
hconSeurat <- FindClusters(hconSeurat, verbose = FALSE)
hconSeurat <- RenameIdents(object = hconSeurat, '0'="1", '1'="2", '2'="3", '3'="4", '4'="5", '5'="6", '6'="7", '7'="8", '8'="9", '9'="10", '10'="11", '11'="12", '12'="13", '13'="14", '14'="15", '15'="16", '16'="17", '17'="18", '18'="19")
jpeg(file="clusterHcEmbryoDiemPca20.jpg", width=1200, height=1000)
DimPlot(hconSeurat, label = TRUE, label.size=8) + NoLegend()
dev.off()
jpeg(file="clustersVln.jpg", width=1200, height=1000)
VlnPlot(hconSeurat, features = c("nFeature_RNA"), pt.size = 0.5)
dev.off()

clusters <- as.numeric(hconSeurat@meta.data$seurat_clusters)
names(clusters) <- colnames(hconSeurat@assays$RNA@counts)
write.table(clusters, "clustersHcEmbryoDiem.txt")

saveRDS(hconSeurat, file = paste("seuratHcEmbryoDiem_pca", pcaDim, ".sct.rds", sep=""))
dim(hconSeurat) # 13687 14128

trss <- list()
uniqueClusters <- unique(clusters)
for (i in 1:length(uniqueClusters)) {
    trss[[i]] <- rowSums(hconSeurat@assays$RNA@counts[,clusters[clusters == i]])
    write.table(trss[[i]], paste("transcriptionHcEmbryo",i,".txt",sep=""))
}

#########################################
# Extracting marker genes
pcaDim <- 20
hconSeurat <- readRDS(paste("seuratHcEmbryoDiem_pca", pcaDim, ".sct.rds", sep=""))

seurat.sce <- as.SingleCellExperiment(hconSeurat)
markersScranAll <- findMarkers(seurat.sce, seurat.sce$seurat_clusters, pval.type="all")

markersPlus <- list()
for (i in 1:length(markersScranAll)) {
   markersPlus[[i]] <- markersScranAll[[i]]@rownames[markersScranAll[[i]]@listData$FDR < 0.001 & markersScranAll[[i]]@listData$summary.logFC > 0]
}
markersMinus <- list()
for (i in 1:length(markersScranAll)) {
   markersMinus[[i]] <- markersScranAll[[i]]@rownames[markersScranAll[[i]]@listData$FDR < 0.001 & markersScranAll[[i]]@listData$summary.logFC < 0]
}
for (i in 1:length(markersScranAll)) {
    write.table(markersPlus[[i]]  , paste("Embryo_marker.plus",i,".txt",sep=""))
}
for (i in 1:length(markersScranAll)) {
    write.table(markersMinus[[i]]  , paste("Embryo_marker.minus",i,".txt",sep=""))
}

#############################
# Annotate using C. elegans
pcaDim <- 20
#sceCele <- readRDS("sceCeEmbryo.rds")
#hconSeurat <- readRDS(paste("seuratHcEmbryoDiem_pca", pcaDim, ".sct.rds", sep=""))
dim(sceCele)
dim(hconSeurat)

varGenesHcon <- head(VariableFeatures(hconSeurat), 2000)

refMatCeleSce <- object_ref(input = sceCele, method = "mean", cluster_col = "cell_type1", var_genes_only = TRUE)
resHcon <- clustify(
  input = hconSeurat,
  cluster_col = "seurat_clusters", # name of column in meta.data containing cell clusters
  ref_mat = refMatCeleSce, # matrix of RNA-seq expression data for each cell type
  query_genes = varGenesHcon, # list of highly varible genes identified with Seurat
  threshold = "auto",
  n_perm=1000,
  compute_method="spearman"
)

clusters <- cbind(colnames(hconSeurat), resHcon@meta.data$type)
colnames(clusters) <- c("cellId", "cellType")
clusters <- data.frame(clusters)

cell.types <- split(clusters, f=clusters$cellType)
for (i in 1:length(cell.types)) {
    cell.types[[i]] <- cell.types[[i]]$cellId
}
cells <- cell.types

#names(cells) <- c("Arcade cells","Body wall muscle","Ciliated amphid neuron","Ciliated non-amphid neurons","Excretory cell parent","Hypodermis","Intestine","Parent of excretory duct pore","Seam cells","T cells","unassigned")
colours <- rainbow(length(cells))

jpeg(file="clustersClustifyMedianHconFromCele.jpg", width=1200, height=1000)
DimPlot(hconSeurat, label = FALSE, cells.highlight = cells, cols.highlight=colours, sizes.highlight=0.2)
dev.off()


