#!/usr/bin/env python

import os, sys, optparse

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i filename -n size')
    parser.add_option('-i', '--matrix', dest='matrix', help='FASTA file to filter', metavar='MATRIX', default='')
    parser.add_option('-c', '--cells', dest='cells', help='Minimum accepted sequence size (default=30)', metavar='CELLS', default='')
    parser.add_option('-f', '--features', dest='features', help='Longest first (default=FALSE)', metavar='FEAS', default='')
    parser.add_option('-g', '--geneIds', dest='geneIds', help='Gene identifiers to keep', metavar='GENEIDS', default='')
    #parser.add_option('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    options, args = parser.parse_args()
    if options.matrix == '':
        parser.print_help()
        sys.exit(1)
    return options


#################################################
def main():
    '''
    '''
    opts = options()
    #patterns = opts.patterns.strip('"').strip("'").split()
    cnt = 0
    idx = 1
    dIdxs = {}
    dGeneIdxs = {}
    dGenes = {}
    with open(opts.geneIds) as handle: # Gene identifiers to keep
        for line in handle:
            dGenes[line.strip()] = True

    with open("barcodes.tsv", 'w') as handleW:
        with open(opts.cells) as handle:
            handle.readline()
            for line in handle:
                items = line.strip().split(',')
                #print (items)
                cnt += 1
                #for pattern in patterns:
                #    print (pattern)
                #    if pattern in items[4]:
                handleW.write("%s\n" %line.split(',')[0].strip('"'))
                dIdxs[cnt] = idx
                idx += 1
                #break

    idx = 1
    cnt = 0
    with open("features.tsv", 'w') as handleW:
        with open(opts.features) as handle:
            handle.readline()
            for line in handle:
                cnt += 1
                items = line.strip().split(',')
                try:
                    geneId = items[0].strip('"')
                    dGenes[geneId]
                    handleW.write("%s\t%s\tGene Expression\n" %(geneId, items[1].strip('"')))
                    dGeneIdxs[cnt] = idx
                    idx += 1
                except KeyError:
                    pass

    cnt = 0
    maxGeneIdx, maxCellIdx = 0, 0
    with open(opts.matrix) as handle:
        handle.readline()
        handle.readline()
        for line in handle:
            items = line.strip().split(' ')
            geneIdx = int(items[0])
            cellIdx = int(items[1])
            try:
                newGeneIdx = dGeneIdxs[geneIdx]
                newCellIdx = dIdxs[cellIdx]
                cnt += 1
                if newGeneIdx > maxGeneIdx: maxGeneIdx = newGeneIdx
                if newCellIdx > maxCellIdx: maxCellIdx = newCellIdx
            except KeyError:
                pass

    with open("matrix.mtx", 'w') as handleW:
        with open(opts.matrix) as handle:
            handleW.write(handle.readline())
            handle.readline()
            handleW.write("%d %d %d\n" %(maxGeneIdx, maxCellIdx, cnt))
            for line in handle:
                items = line.strip().split(' ')
                geneIdx = int(items[0])
                cellIdx = int(items[1])
                try:
                    newGeneIdx = dGeneIdxs[geneIdx]
                    newCellIdx = dIdxs[cellIdx]
                    geneIdx = int(items[0])
                    value = int(items[2])
                    handleW.write("%d %d %d\n" %(newGeneIdx, newCellIdx, value))
                except KeyError:
                    pass


#################################################
if __name__ == "__main__":
    main()
