#!/bin/bash

############################################################################################
##### Create reciprocal hits between non-isoformic protein from H. contortos and C. elegans
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/985/GCF_000002985.6_WBcel235/GCF_000002985.6_WBcel235_genomic.fna.gz
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/985/GCF_000002985.6_WBcel235/GCF_000002985.6_WBcel235_genomic.gff.gz
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/985/GCF_000002985.6_WBcel235/GCF_000002985.6_WBcel235_protein.faa.gz
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/985/GCF_000002985.6_WBcel235/GCF_000002985.6_WBcel235_cds_from_genomic.fna.gz

wget https://ftp.ebi.ac.uk/pub/databases/wormbase/parasite/releases/WBPS14/species/haemonchus_contortus/PRJEB506/haemonchus_contortus.PRJEB506.WBPS14.annotations.gff3.gz
wget https://ftp.ebi.ac.uk/pub/databases/wormbase/parasite/releases/WBPS14/species/haemonchus_contortus/PRJEB506/haemonchus_contortus.PRJEB506.WBPS14.genomic_softmasked.fa.gz
wget https://ftp.ebi.ac.uk/pub/databases/wormbase/parasite/releases/WBPS14/species/haemonchus_contortus/PRJEB506/haemonchus_contortus.PRJEB506.WBPS14.protein.fa.gz

gunzip *.gz
ln -s GCF_000002985.6_WBcel235_cds_from_genomic.fna cel.cds.fa
awk '!/>/{s=s""$0};/>/{if (length(s)>=30) print s"\n"$1; s=""}END{print s}' GCF_000002985.6_WBcel235_protein.faa | sed 1,1d > cel.pts.fa
awk '!/>/{s=s""$0};/>/{if (length(s)>=30) print s"\n"$1;s=""}END{print s}' haemonchus_contortus.PRJEB506.WBPS14.protein.fa | sed 1,1d > hco.pts.fa

python removeIsoforms.py -i cel.cds.fa > cel.gene.pts.mapping
awk '{print $2}' cel.gene.pts.mapping | awk '{print ">"$0}' | fgrep -A 1 -x -f - -i cel.pts.fa | sed '/-[^-]*-/d' > cel.pts.noisoforms.fa
grep ">" hco.pts.fa | grep -v -E ".[2-9]+$" | fgrep -A 1 -x -f - -i hco.pts.fa | sed '/-[^-]*-/d' > hco.pts.noisoforms.fa

makeblastdb -in cel.pts.noisoforms.fa -dbtype prot
blastp -db cel.pts.noisoforms.fa -query hco.pts.noisoforms.fa -evalue 1e-8 -num_threads 24 -outfmt 6 | awk '!a[$1]++' > hco.pts.blast
makeblastdb -in hco.pts.noisoforms.fa -dbtype prot
blastp -db hco.pts.noisoforms.fa -query cel.pts.noisoforms.fa -evalue 1e-8 -num_threads 24 -outfmt 6 | awk '!a[$1]++' > cel.pts.blast
awk '{print $2"\t"$1}' hco.pts.blast | fgrep -w -f - cel.pts.blast > reciprocal.hits

awk 'BEGIN{while ((getline < "cel.gene.pts.mapping") > 0) a[$2]=$1}{print a[$1]"\t"$2}' reciprocal.hits > cel2Hco.mapping
awk '{print $1}' cel2Hco.mapping > cel2Hco.ids
awk '{print $2}' cel2Hco.mapping | sed 's/transcript://1' > hco.ids


############################################################################################
##### Fetch annotations for C. elegans single cell experiment
##### Data from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE126954
##### From publication https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7428862
wget https://ftp.ncbi.nlm.nih.gov/geo/series/GSE126nnn/GSE126954/suppl/GSE126954_cell_annotation.csv.gz
wget https://ftp.ncbi.nlm.nih.gov/geo/series/GSE126nnn/GSE126954/suppl/GSE126954_gene_annotation.csv.gz
wget https://ftp.ncbi.nlm.nih.gov/geo/series/GSE126nnn/GSE126954/suppl/GSE126954_gene_by_cell_count_matrix.txt.gz
gunzip *.gz
mkdir -p MappedToHco
python divideByGenes.py -i GSE126954_gene_by_cell_count_matrix.txt -c GSE126954_cell_annotation.csv -f GSE126954_gene_annotation.csv -g cel2Hco.ids
mv matrix.mtx MappedToHco
mv barcodes.tsv MappedToHco
awk -F"\t" 'BEGIN{while ((getline < "cel2Hco.mapping") > 0) a[$1]=$2}{print a[$2]"\t"$1"\t"$3}' features.tsv | awk -F"\t" '{print $1"\t"$1"\t"$3}' | sed -E '/^\t/d' > features.rn.tsv
mv features.rn.tsv MappedToHco/features.tsv
rm features.tsv
gzip MappedToHco/*

############################################################################################
##### Processing single nuclei data for H. contortus embryo
# gff2gtf from https://github.com/dzmitrybio/gff2gtf
wget https://raw.githubusercontent.com/dzmitrybio/gff2gtf/master/gff2gtf.awk
awk -f gff2gtf.awk haemonchus_contortus.PRJEB506.WBPS14.annotations.gff3 > hco.gtf
awk '{if ($3!="gene") print $0}' hco.gtf | sed 's/mRNA/transcript/1' | awk -F["\t",";"] '{if ($3=="transcript") print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$10"; "$9";"; else print $0}' | grep -v -E "five_|three_" > hco.tmp.gtf
sed 's/gene://g;s/transcript://g;s/exon://g;s/cds://g' hco.tmp.gtf | awk -F["\t",";"] '{if ($3=="exon") print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9";"$10";"$11";"; else print $0}' | awk -F["\t",";"] '{if ($3=="CDS") print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9";"$10";"; else print $0}' | sed 's/\.[0-9]*//g' | awk -F[" "] '{if (/exon/) {print $1" "$2" "$3" "$4" "$5" "$6" "$7" "$8"exon_id "$4 $6} else print $0}' | sed 's/";"/./g' | awk '{if ($3!="transcript") {print $0" gene_biotype \"protein_coding\";"} else print $0}' | sed 's/ exon_id/exon_id/1' | awk -F"\t" '{if ($3=="transcript") {print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t.\t"$7"\t.\t"$9; s=$4; e=$5} if ($3=="exon") print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t.\t"$7"\t.\t"$9; if ($3=="CDS") print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t.\t"$7"\t"$8"\t"$9}' > hco.tmp2.gtf

awk -F"\t" '{if ($3=="transcript") {if (c==1&&$7=="+") {print p1"\t"p2"\tstart_codon\t"s"\t"s+2"\t.\t"p7"\t"p8"\t"p9; print p1"\t"p2"\tend_codon\t"e-2"\t"e"\t.\t"p7"\t"p8"\t"p9} if (c==1&&$7=="-") {print p1"\t"p2"\tstart_codon\t"e-2"\t"e"\t.\t"p7"\t"p8"\t"p9; print p1"\t"p2"\tend_codon\t"s"\t"s+2"\t.\t"p7"\t"p8"\t"p9} print $0; p1=$1; p2=$2; p7=$7; p8=$8; p9=$9; c==0; s=$4; e=$5} if ($3=="exon") {print $0; c=1} if ($3=="CDS") {print $0}}END{if (p7=="+") {print p1"\t"p2"\tstart_codon\t"s"\t"s+2"\t.\t"p7"\t"p8"\t"p9; print p1"\t"p2"\tend_codon\t"e-2"\t"e"\t.\t"p7"\t"p8"\t"p9} if (p7=="-") {print p1"\t"p2"\tstart_codon\t"e-2"\t"e"\t.\t"p7"\t"p8"\t"p9; print p1"\t"p2"\tend_codon\t"s"\t"s+2"\t.\t"p7"\t"p8"\t"p9}}' hco.tmp2.gtf | awk '{if ($3!="start_codon"&&$3!="end_codon") print $0}' > hco.final.gtf

cellranger mkref --genome=HconSangerExons \
                 --fasta=haemonchus_contortus.PRJEB506.WBPS14.genomic_softmasked.fa \
                 --genes=hco.final.gtf \
                 --ref-version=1.0
cellranger count --include-introns true --no-bam --id=hcoEmbryoExons_150k --fastqs=<directory for the files in SRR24235293-SRR24235296> --transcriptome=/home/pakorhon/Software/Diem/Final/HconSangerExons --localcores=94 --force-cells=150000

############################################################################################
##### Prepare clusters based on genes homologous with C. elegans and annotate them
Rscript runEmbryo.r

############################################################################################
##### Inspect the relationship of essential genes among clusters
Rscript runEssential.r

awk 'BEGIN{while ((getline<"selectedHcos.rn.ids")>0) a[$1]="*"}{print $1"\t"$2"\t"a[$2]}' cel2Hco.rn.mapping > cel2Hco.tsv

python2 ~/Codebase/joiner.py -i "eleven1.txt eleven2.txt eleven3.txt eleven4.txt eleven5.txt eleven6.txt eleven7.txt eleven8.txt eleven9.txt eleven10.txt eleven11.txt eleven12.txt eleven13.txt eleven14.txt eleven15.txt eleven16.txt eleven17.txt eleven18.txt eleven19.txt" > elevenGenes.cnts
