#!/usr/bin/env python

import os, sys, optparse, re

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i filename -n size')
    parser.add_option('-i', '--fasta', dest='fasta', help='FASTA file to filter', metavar='FASTA', default='')
    options, args = parser.parse_args()
    if options.fasta == '':
        parser.print_help()
        sys.exit(1)
    return options

#################################################
def main():
    '''
    '''
    opts = options()
    dProts = {}
    with open(opts.fasta) as handle:
        for line in handle:
            if line[0] != ">": continue
            items = re.findall('\[[^\]]*\]|\([^\)]*\)|\"[^\"]*\"|\S+', line.strip())
            geneId, protId = None, None
            for i in range(len(items)):
                if "db_xref=EnsemblGenomes-Gn:" in items[i]:
                    blocks = items[i].split(',')
                    if len(blocks) == 1:
                        print ("Did not succeed!")
                        sys.exit(0)
                    for block in blocks:
                        if "db_xref=EnsemblGenomes-Gn:" in block:
                            geneId = block.split(':')[-1]
                if "protein_id=" in items[i]:
                    protId = items[i].split('=')[-1].strip(']')
            if protId != None:
                try:
                    dProts[geneId].append(protId)
                except KeyError:
                    dProts[geneId] = []
                    dProts[geneId].append(protId)
    for key in dProts:
        found = False
        if key == None: continue
        for item in dProts[key]:
            if item[-2:] == ".1":
                print ("%s\t%s" %(key, item))
                found = True
        if found == False:
            print ("%s\t%s" %(key, dProts[key][0]))

#################################################
if __name__ == "__main__":
    main()
